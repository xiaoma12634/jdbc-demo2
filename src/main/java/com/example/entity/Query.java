package com.example.entity;

import lombok.Data;

@Data
public class Query {
    String sql;
}
